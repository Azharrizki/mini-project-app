<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class AuthController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Login'
        ];

        return view('auth.index', $data);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        $req = Http::post('http://localhost:8001/login', $data);

        $res = json_decode($req->body());

        if ($res->status === true) {
            Alert::success('Selamat!', $res->message);

            Session::put('token', $res->token);
            return redirect()->to('/dashboard');
        } else {
            Alert::error('Oops!', $res->message);

            return redirect()->back();
        }
    }
}
