<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ItemController extends Controller
{
    public function index()
    {
        $response = Http::get('http://localhost:8001/item');

        $getItem = json_decode($response->body());

        $data = [
            'title' => 'Data Item',
            'data' =>  $getItem
        ];


        return view('item.index', $data);
    }

    public function upload(Request $request)
    {
        $req = Http::attach('attachment', file_get_contents($request->file('csv'), 'r'))
            ->post('http://localhost:8001/upload-csv', [
                'username' => $request->input('username'),
                'csv' => fopen($request->file('csv'), 'r'),
                'user_id' => $request->input('user_id')
            ]);

        $res = json_decode($req->body());

        if ($res->status == true) {
            Alert::success('Selamat', $res->message);
            return redirect()->back();
        } else {
            Alert::error('Oops!', $res->message);
            return redirect()->back();
        }
    }
}
