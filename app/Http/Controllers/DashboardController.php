<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DashboardController extends Controller
{
    public function index()
    {
        $getHistory = Http::get('http://localhost:8001/history');
        // $getItem = Http::get('http://localhost:8001/item');
        $getUser = Http::get('http://localhost:8001/user');

        $resHistory = json_decode($getHistory->body());
        // $resItem = json_decode($getItem->body());
        $resUser = json_decode($getUser->body());

        $data = [
            'title' => 'Dashboard',
            'history' => $resHistory->data,
            // 'item' => $resItem->data,
            'user' => $resUser->data
        ];

        return view('dashboard.index', $data);
    }
}
