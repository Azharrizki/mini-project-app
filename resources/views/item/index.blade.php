@extends('layout.index')

@section('section')
    <section class="section">
        <div class="section-header">
            <h1>Mini Project</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Bootstrap Components</a></div>
                <div class="breadcrumb-item">Form</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('upload') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4>Custom Forms</h4>
                            </div>
                            <div class="card-body">
                                <input type="hidden" name="user_id" value="1">
                                <input type="hidden" name="username" value="azharrizki">
                                <div class="section-title mt-0">Upload CSV</div>
                                <div class="d-flex">
                                    <div class="custom-file col-4">
                                        <input type="file" class="custom-file-input" name="csv" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>

                                    <button type="submit" class="btn btn-success ml-2">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table id="myTable" class="table">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Barang</th>
                                        <th class="text-center">Deskripsi</th>
                                        <th class="text-center">Stok</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach ($data->data as $item)
                                        <tr>
                                            <td class="text-center">{{ $i++ }}</td>
                                            <td class="text-center">{{ $item->nama }}</td>
                                            <td class="text-center">{{ $item->deskripsi }}</td>
                                            <td class="text-center">{{ $item->stok }}</td>
                                            <td class="text-center">
                                                <a href="" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
